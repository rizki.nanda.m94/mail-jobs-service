<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Email;
use Faker\Generator as Faker;

$factory->define(Email::class, function (Faker $faker) {
    return [
        'from' => 'admin@mail.com',
        'to' => $faker->unique()->safeEmail,
        'subject' => $faker->sentence(10),
        'attach' => null,
        'multiple' => null,
        'cc' => null,
        'bc' => null,
        'status' => 0
    ];
});
