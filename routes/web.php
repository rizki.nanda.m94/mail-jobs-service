<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/user/{user}', 'HomeController@show');


// Route::get('/{id}', 'PostController@index');

// Route::get('/seed', function(\App\User $user){
//     $faker = Faker\Factory::create();

//     foreach(range(5, 50) as $x){
//         $user->create([
//             'username' => $faker->sentence(1),
//             'first_name' => $faker->sentence(1),
//             'last_name' => $faker->sentence(1),
//             'email' => $faker->sentence(1).'@codepolitan.com',
//             'password' => $faker->sentence(1),
//             'age' => rand(2, 30)
//         ]);
//     }
// });
Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');

