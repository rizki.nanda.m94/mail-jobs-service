<?php

namespace App\Jobs;

use Illuminate\Support\Facades\Mail;
use App\Email;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SendmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     *
     */

    protected $id_email = null;
    protected $path_temp = null;

    public function __construct($id_email)
    {
       $this->id_email = $id_email;

    //    $emails = Email::where([
    //        ['id', $this->id_email],
    //        ['status', 0]
    //     ])->get();
    //    $emails_arr = json_decode($emails);

    //     foreach($emails_arr as $data){
    //         echo $data->attach;
    //         $attachments = json_decode($data->attach);
    //         $this->path_temp = $attachments;
    //     }
    //     dd($this->path_temp);

    //     die('tess');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::error("ini adalah Log alert :))))");
        $emails = Email::where([
                        ['id', $this->id_email],
                        ['status', 0]
                    ])->get();

        $emails_arr = json_decode($emails);

        foreach($emails_arr as $data){
            Mail::html("{$data->message}", function($mail) use ($data){
                $mail->from($data->from);
                $mail->to($data->to)
                ->subject($data->subject);

                $attachments = json_decode($data->attach);
                $this->path_temp = $attachments;

                if(is_array($attachments)){
                    foreach($attachments as $filePath){
                        $mail->attach($filePath);
                    }
                }else{
                    $mail->attach($attachments);
                    // $mail->attach('D:\xampp-7\htdocs\belajarLaravel\public\uploads\tes.pdf', ['as' => 'tesss.pdf', 'mime' => 'application/pdf']);
                }

            });
            sleep(3); //jeda selama 3 detik setiap send mail

            $email = Email::find($data->id);
            $email->status = 1;
            $email->save();
        }

        if(count(Mail::failures()) > 0) {
            Log::alert("There was one or more failures. They were: <br />");
            foreach(Mail::failures() as $email_address) {
                Log::alert("- $email_address <br />");
             }
         } else {
            if(is_array($this->path_temp)){
                foreach($this->path_temp as $tmp){
                   unlink($tmp);
                }
            }else{
                unlink($this->path_temp);
            }

            // unlink('D:\xampp-7\htdocs\belajarLaravel\public\uploads\tes.pdf');
            // unlink('D:\xampp-7\htdocs\belajarLaravel\public\uploads\tes.pdf'); //unlink file attachment


            //  echo "No errors, all sent successfully!";
             Log::alert("No errors, all sent successfully!");
         }
    }
}
