<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index(Request $request)
    {   

        $post = Post::find($request->id);

        // dd($post, $post->published_at);

        return view('post.show', [
            'post' => $post
        ]);

        // $post = Post::where('is_published', 1)->get();
        // $post = Post::orderBy('id', 'desc')->get();
        // $post = Post::latest()->limit(3)->get(); // latest() ==> sama dengan oerderBy(created_date, desc)  
        // take() == limit()

        // $post = Post::findOrFail($request->id); //(findorFail) page not found (404) jika url/id tidak ditemukan
        
        // $post->delete();

        // $post->update([
        //     'title' => 'title satu',
        //     'content' => 'content satu',
        // ]);

        // atau

        // $post->title = 'title 1';
        // $post->content = 'content 1';
        // $post->save();

        /// --get data (eloquent)--- ///
        // $posts = Post::take(5)->get();
        // return view('post.index', ['posts' => $posts]);

        /// --- method create (harus mengisi fillable / guarded di model) ---- /// 
        // Post::create([      
        //     'title' => 'title 2',
        //     'content' => 'Consequat tempor cupidatat consectetur nostrud consequat esse culpa Lorem.'
        // ]);

        /// --- method save(tidak harus mengisi fillable / guarded di model) ---- ///
        // $post = new Post();
        // $post->title = 'title 2';
        // $post->content = 'Est sunt Lorem amet deserunt cupidatat sit occaecat aliquip non labore.';
        // $post->save();
        

            
    }
}
