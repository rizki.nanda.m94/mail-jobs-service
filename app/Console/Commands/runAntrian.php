<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Email;
use App\Jobs\SendmailJob;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Mail;

class runAntrian extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'antrian:run {--id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Menjalankan antrian email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id_email = $this->option('id');
        // return $id_email;

        /// ---with params data in command--- ///
        // $dt_arr = [];
        // foreach(explode(",", $datas) as $data) {
        //     list($key, $value) = explode(":", $data);
        //     $dt_arr[$key] = $value;
        // }
        // dd($dt_arr);
        /// ---end--- ///

        dispatch(new SendmailJob($id_email))->delay(now()->addSeconds(5));
    }
}
