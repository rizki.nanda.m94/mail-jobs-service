<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title', 
        'content',
    ]; 

    protected $dates = [
        'published_at'
    ];

    //  atau
    //  protected $guarded = []; 
    //  (jika fieldnya banyak)
}
